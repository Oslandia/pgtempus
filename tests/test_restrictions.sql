---------------------------------------------
--
--           Restrictions
--
---------------------------------------------


--
-- build a simple graph to test restrictions
create table test_restriction_edges (
       id int8,
       node_from_id int8,
       node_to_id int8,
       initial_cost int4
);

insert into test_restriction_edges (id, node_from_id, node_to_id, initial_cost)
values
  (1, 1, 2, 10),
  (2, 2, 3, 10),
  (3, 2, 5, 10), -- the "shortcut"
  (4, 3, 4, 10),
  (5, 4, 5, 10),
  (6, 5, 6, 10),
  (7, 5, 2, 10000) -- reverse of the shortcut
;

select tempus_build_multimodal_graph('r',
-- transport modes
$sql$
  select 1::int8 as id
  , 'Walking'::text as name
  , 'Walking'::text as category
  , '{1}'::int2[] as traffic_rules
$sql$,
-- nodes
$sql$
  select
    id::int8
    , 0.0::float4 as x
    , 0.0::float4 as y
    , 0.0::float4 as z
    , 0::int2 as parking_transport_modes
  from generate_series(1,7) id
$sql$,
-- edges
$sql$
  select
   id::int8
   , node_from_id
   , node_to_id
   , 'f'::bool as is_pt
   , '{1}'::int2[] as traffic_rules
  from test_restriction_edges
$sql$
);

select tempus_set_static_road_edge_cost('r', $sql$
  select
    e.id::int8 as edge_id,
    1::int4 as mode_id,
    initial_cost as duration,
    0.::float4 as additional_cost
  from
    test_restriction_edges e
$sql$,
1.0 -- time cost weight
);

-- add a penalty of 10 to the shortcut
select tempus_set_cost_restrictions('r',
$sql$
  select '{1,3,6}'::int8[] as edge_sequence, 1::int8 as mode_id, 10::int4 as time, .0::float4 as cost
$sql$
);

select test.one_to_many_node_paths('r', 1, '{6}', '{1}', '{"{1,2,5,6} - 40 - 00:00:40"}', 'Path with a weak restriction');
select test.many_to_one_node_paths('r', 6, '{1}', '{1}', '2000-1-1 12:00', '{"{1,2,5,6} - 43160 - 11:59:20"}', 'Path with a weak restriction, before 12:00');

-- set a stronger penalty, so that the shortcut is excluded from the best path
select tempus_set_cost_restrictions('r',
$sql$
  select '{1,3,6}'::int8[] as edge_sequence, 1::int8 as mode_id, 40::int4 as time, .0::float4 as cost
$sql$
);

select test.one_to_many_node_paths('r', 1, '{6}', '{1}', '{"{1,2,3,4,5,6} - 50 - 00:00:50"}', 'Path with a stronger restriction');
select test.many_to_one_node_paths('r', 6, '{1}', '{1}', '2000-1-1 12:00', '{"{1,2,3,4,5,6} - 43150 - 11:59:10"}', 'Path with a stronger restriction, before 12:00');

-- lower the cost of the reverse shortcut to cut the restriction sequence
select tempus_set_static_road_edge_cost('r', $sql$
  select
    e.id::int8 as edge_id,
    1::int4 as mode_id,
    1::int4 as duration,
    0.::float4 as additional_cost
  from
    test_restriction_edges e
  where
    id = 7
$sql$,
1.0 -- time cost weight
);

select test.one_to_many_node_paths('r', 1, '{6}', '{1}', '{"{1,2,5,2,5,6} - 41 - 00:00:41"}', 'Path that goes partly through a restriction sequence');
select test.many_to_one_node_paths('r', 6, '{1}', '{1}', '2000-1-1 12:00', '{"{1,2,5,2,5,6} - 43159 - 11:59:19"}', 'Path that goes partly through a restriction sequence, before 12:00');
