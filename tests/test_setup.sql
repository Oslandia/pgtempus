select pg_backend_pid();

drop schema if exists test cascade;
create schema test;

create function test.one_to_one_node_path(graph_id text, source_id int8, target_id int8, modes int8[], expected_result text, msg text)
returns text
language sql
as
$$
with recursive roadmap as (
  select * from tempus_one_to_many_paths(graph_id, 'multimodal', source_id, array[target_id], null, '2000-1-1', modes)
)
, tpath as (
  select
    id,
    predecessor_id,
    transport_mode_id as path_id,
    array[node_from_id, node_to_id] as path,
    accumulated_cost,
    arrival_time
  from roadmap where node_to_id = target_id
  union all
  select
    r.id
    , r.predecessor_id,
    t.path_id,
    r.node_from_id || path,
    t.accumulated_cost,
    t.arrival_time
  from
    roadmap r
    join tpath t on r.id = t.predecessor_id
    where r.predecessor_id > 0
)
select
  is(array_agg(path::text || ' - ' || accumulated_cost || ' - ' || arrival_time order by path_id)::text,
    expected_result,
    msg)
from
(
select
  row_number() over (partition by path_id order by accumulated_cost) as n,
  count(1) over (partition by path_id) as c,
  *
from tpath
) t
where c = n;
$$;

create function test.one_to_many_node_paths(graph_id text, source_id int8, target_ids int8[], modes int8[], expected_result text, msg text)
returns text
language sql
as
$$
with recursive roadmap as (
select * from tempus_one_to_many_paths(graph_id, 'multimodal', source_id, target_ids, null, '2000-1-1', modes)
)
, tpath as (
  select
    id,
    predecessor_id,
    target_id,
    transport_mode_id,
    array[node_from_id, node_to_id] as path,
    accumulated_cost,
    arrival_time
  from
    roadmap
    join unnest(target_ids) target_id on node_to_id = target_id
  union all
  select
    r.id
    , r.predecessor_id,
    t.target_id,
    t.transport_mode_id,
    r.node_from_id || path,
    t.accumulated_cost,
    t.arrival_time
  from
    roadmap r
    join tpath t on r.id = t.predecessor_id
    where r.predecessor_id > 0
)
select
  is(array_agg(result)::text,
     expected_result,
     msg)
from
(
select
  path::text || ' - ' || accumulated_cost || ' - ' || arrival_time as result
from
  tpath
  , (
    select
      target_id
      , transport_mode_id
      , max(array_length(path,1)) as max_length
    from tpath
    group by target_id, transport_mode_id
  ) t
where
  tpath.target_id = t.target_id
  and tpath.transport_mode_id = t.transport_mode_id
  and array_length(tpath.path, 1) = max_length
order by
  tpath.target_id, tpath.transport_mode_id
) r;
$$;


create function test.many_to_one_node_paths(graph_id text, source_id int8, target_ids int8[], modes int8[], before_dt timestamp, expected_result text, msg text)
returns text
language sql
as
$$
with recursive roadmap as (
select * from tempus_many_to_one_paths(graph_id, 'multimodal', source_id, target_ids, null, before_dt, modes)
)
, tpath as (
  select
    id,
    predecessor_id,
    target_id,
    transport_mode_id,
    array[node_from_id, node_to_id] as path,
    accumulated_cost,
    departure_time
  from
    roadmap
    join unnest(target_ids) target_id on node_from_id = target_id
  union all
  select
    r.id
    , r.predecessor_id,
    t.target_id,
    t.transport_mode_id,
    path || r.node_to_id,
    t.accumulated_cost,
    t.departure_time
  from
    roadmap r
    join tpath t on r.id = t.predecessor_id
    where r.predecessor_id > 0
)
select
  is(array_agg(result)::text,
     expected_result,
     msg)
from
(
select
  path::text || ' - ' || accumulated_cost || ' - ' || departure_time as result
from
  tpath
  , (
    select
      target_id
      , transport_mode_id
      , max(array_length(path,1)) as max_length
    from tpath
    group by target_id, transport_mode_id
  ) t
where
  tpath.target_id = t.target_id
  and tpath.transport_mode_id = t.transport_mode_id
  and array_length(tpath.path, 1) = max_length
order by
  tpath.target_id, tpath.transport_mode_id
) r;
$$;

