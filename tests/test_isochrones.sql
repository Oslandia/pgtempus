---------------------------------------------
--
--           Isochrones
--
---------------------------------------------

create table test_isochrone_edges as
with l1 as (
select
  1 as node_from_id
  ,t as node_to_id
from
  generate_series(2,5) t
)
, l2 as (
select
  parent.node_to_id as node_from_id
  , ((parent.node_to_id - 1) << 2) + t + 2 as node_to_id
from
  l1 as parent
  , generate_series(0,3) t
)
, l3 as (
select
  parent.node_to_id as node_from_id
  , ((parent.node_to_id - 1) << 2) + t + 2 as node_to_id
from
  l2 as parent
  , generate_series(0,3) t
)
select
  row_number() over () as id
  , node_from_id
  , node_to_id
  , 10::int4 as cost
from
(
select * from l1
union all
select * from l2
union all
select * from l3
) t
;

select tempus_build_multimodal_graph('gi',
-- transport modes
$sql$
  select 1::int8 as id
  , 'Walking'::text as name
  , 'Walking'::text as category
  , '{1}'::int2[] as traffic_rules
$sql$,
-- nodes
$sql$
  select
    id::int8
    , 0.0::float4 as x
    , 0.0::float4 as y
    , 0.0::float4 as z
    , 0::int2 as parking_transport_modes
  from generate_series(1,(select max(node_to_id) from test_isochrone_edges)) id
$sql$,
-- edges
$sql$
  select
   id::int8
   , node_from_id::int8
   , node_to_id::int8
   , 'f'::bool as is_pt
   , '{1}'::int2[] as traffic_rules
  from test_isochrone_edges
$sql$
);

select tempus_set_static_road_edge_cost('gi', $sql$
  select
    e.id::int8 as edge_id,
    1::int4 as mode_id,
    cost as duration,
    0.::float4 as additional_cost
  from
    test_isochrone_edges e
$sql$,
1.0 -- time cost weight
);

select $$(1,0,0,0,0,0,1,,00:00:00,00:00:00,00:00:00)
 (2,1,1,1,2,10,1,,00:00:00,00:00:00,00:00:10)
 (3,1,2,1,3,10,1,,00:00:00,00:00:00,00:00:10)
 (4,1,3,1,4,10,1,,00:00:00,00:00:00,00:00:10)
 (5,1,4,1,5,10,1,,00:00:00,00:00:00,00:00:10)
 (6,2,5,2,6,20,1,,00:00:00,00:00:10,00:00:20)
 (7,2,6,2,7,20,1,,00:00:00,00:00:10,00:00:20)
 (8,2,7,2,8,20,1,,00:00:00,00:00:10,00:00:20)
 (9,2,8,2,9,20,1,,00:00:00,00:00:10,00:00:20)
 (10,3,9,3,10,20,1,,00:00:00,00:00:10,00:00:20)
 (11,3,10,3,11,20,1,,00:00:00,00:00:10,00:00:20)
 (12,3,11,3,12,20,1,,00:00:00,00:00:10,00:00:20)
 (13,3,12,3,13,20,1,,00:00:00,00:00:10,00:00:20)
 (14,4,13,4,14,20,1,,00:00:00,00:00:10,00:00:20)
 (15,4,14,4,15,20,1,,00:00:00,00:00:10,00:00:20)
 (16,4,15,4,16,20,1,,00:00:00,00:00:10,00:00:20)
 (17,4,16,4,17,20,1,,00:00:00,00:00:10,00:00:20)
 (18,5,17,5,18,20,1,,00:00:00,00:00:10,00:00:20)
 (19,5,18,5,19,20,1,,00:00:00,00:00:10,00:00:20)
 (20,5,19,5,20,20,1,,00:00:00,00:00:10,00:00:20)
 (21,5,20,5,21,20,1,,00:00:00,00:00:10,00:00:20)$$ as isochrone_result \gset

select results_eq(
  $sql$select row(t.*)::text from tempus_one_to_many_paths('gi', 'multimodal', 1, null, 25.0, '2000-1-1', '{1}') t$sql$,
  $$select * from unnest(string_to_array('$$ || :'isochrone_result' || $$', E'\n '))$$,
'Test isochrone');

--
-- test many to one isochrone
-- 1/ build a graph with reversed edges

select tempus_build_multimodal_graph('r_gi',
-- transport modes
$sql$
  select 1::int8 as id
  , 'Walking'::text as name
  , 'Walking'::text as category
  , '{1}'::int2[] as traffic_rules
$sql$,
-- nodes
$sql$
  select
    id::int8
    , 0.0::float4 as x
    , 0.0::float4 as y
    , 0.0::float4 as z
    , 0::int2 as parking_transport_modes
  from generate_series(1,(select max(node_to_id) from test_isochrone_edges)) id
$sql$,
-- edges
$sql$
  select
   id::int8
   -- reversed here
   , node_to_id::int8
   , node_from_id::int8
   , 'f'::bool as is_pt
   , '{1}'::int2[] as traffic_rules
  from test_isochrone_edges
$sql$
);

select tempus_set_static_road_edge_cost('r_gi', $sql$
  select
    e.id::int8 as edge_id,
    1::int4 as mode_id,
    cost as duration,
    0.::float4 as additional_cost
  from
    test_isochrone_edges e
$sql$,
1.0 -- time cost weight
);

select results_eq(
  $sql$select row(t.*)::text from tempus_many_to_one_paths('r_gi', 'multimodal', 1, null, 25.0, null, '{1}') t$sql$,
  $$select * from unnest(string_to_array('$$ || :'isochrone_result' || $$', E'\n '))$$,
'Test reverse isochrone');
