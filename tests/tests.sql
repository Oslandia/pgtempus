\set ECHO none
\set QUIET 1
-- Turn off echo and keep things quiet.

-- Format the output for nice TAP.
--\pset format unaligned
--\pset tuples_only true
\pset pager

-- Revert all changes on failure.
\set ON_ERROR_ROLLBACK 1
\set ON_ERROR_STOP true

drop extension if exists pgtempus cascade;
create extension pgtempus cascade;

-- Load the TAP functions.
BEGIN;
    \i pgtap/pgtap.sql

\i test_setup.sql

-- Run the tests.
select * from no_plan();

\i test_multimodal.sql
\i test_restrictions.sql
\i test_isochrones.sql

-- Finish the tests and clean up.
select * from finish();
rollback;
