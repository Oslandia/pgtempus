#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -z "$1" ]; then
    DB_CONN="dbname=test_pgtempus"
else
    DB_CONN=$1
fi

pushd $DIR
psql -d "$DB_CONN" -Xf tests.sql
popd
